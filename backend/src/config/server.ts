import bodyParser from 'body-parser';
import cors from 'cors';
import { Request, Response, NextFunction, Application, ErrorRequestHandler } from 'express';
import helmet from 'helmet';
import * as _ from 'lodash';
import routes from '../routes';
import { Exception } from '../exception';


export default (app: Application) => {
  app.enable('trust proxy');
  app.use(cors());
  app.use(helmet());
  app.use(bodyParser.json());

  app.use('/api', routes);

  /// catch 404 and forward to error handler
  app.use((req: Request, res: Response, next) => {
    const error: Error = new Error('Not Found');
    // error.status = 404;
    next(error);
  });

  /// error handlers
  app.use((err: ErrorRequestHandler, req: Request, res: Response, next: NextFunction) => {
    /**
     * Handle 401 thrown by express-jwt library
     */
    if (err.name === 'UnauthorizedError') {
      return res.status(501).send({ message: 'not authorized' }).end();
    }

    if (err.name === 'TestException') {
      return res
        .status(406)
        .send({
          code: _.get(err, 'code', false),
          message: _.get(err, 'message', false),
        })
        .end();
    }


    return next(err);
  });

  app.use((err: Exception, req: Request, res: Response) => {
    const statusCode = err.status || 500;
    res.status(statusCode);
    res.json({
      message: err.message,
      code: statusCode,
    });
  });

  console.info('Server loaded!');
};
