import express, { Request, Response } from 'express';
import config from './config';
import server from './config/server';
import 'reflect-metadata';

const app = express();

async function initialize() {
  server(app);
  app.listen(config.port, () => {
    console.log(`Server is running at ${config.port}`);
  });
}

initialize();

