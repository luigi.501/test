export interface Provider {
    name: string;
    businessName: string;
    address: string;
}