import { Config } from "./Config";
import { Provider } from "./Provider";

export interface SchemaDb {
    info: Config;
    providers: Provider[];
}
