import { promises as fs } from 'fs';
import { Provider } from '../models/Provider';
import { SchemaDb } from '../models/Schema';
import { Config } from '../models/Config';



export async function getAllProviders(): Promise<Provider[]> {
    try {
        const data = await fs.readFile('db.json', 'utf8');
        const jsonData: SchemaDb = JSON.parse(data);
        return jsonData.providers;
    } catch (err) {
        throw new Error(`Error al leer el archivo: ${err.message}`);
    }
}

export async function getInfo(): Promise<Config> {
    try {
        const data = await fs.readFile('db.json', 'utf8');
        const jsonData: SchemaDb = JSON.parse(data);
        return jsonData.info;
    } catch (err) {
        throw new Error(`Error al leer el archivo: ${err.message}`);
    }
}


export async function createProvider(provider: Provider): Promise<Provider> {
    try {
        const data = await fs.readFile('db.json', 'utf8');
        const jsonData: SchemaDb = JSON.parse(data);
        jsonData.providers.push(provider);
    
        const updatedData = JSON.stringify(jsonData, null, 2);
        await fs.writeFile('db.json', updatedData, 'utf8');

        return provider;
    } catch (err) {
        throw new Error(`Error al leer el archivo: ${err.message}`);
    }
}
