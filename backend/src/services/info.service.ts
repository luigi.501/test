7
import { Config } from '../models/Config';
import { getInfo } from '../utils';
import { Service } from 'typedi';

@Service()
export default class InfoService {
    constructor() {}


      public async getInfo(): Promise<Config> {
        return getInfo();
      }

  
}