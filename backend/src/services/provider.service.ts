7
import { createProvider, getAllProviders } from '../utils';
import { Provider } from '../models/Provider';
import { Service } from 'typedi';

@Service()
export default class ProviderService {
    constructor() {}


      public async getAll(): Promise<Provider[]> {
        return getAllProviders();
      }

      public async createProvider(provider: Provider): Promise<Provider> {
        return createProvider(provider);
      }
}