
import { Container } from 'typedi';
import { Request, Response, Router, NextFunction } from 'express';
import ProviderService from '../../services/provider.service';
import { Provider } from '../../models/Provider';

const route = Router();
export default (app: Router) => {
  app.use('/providers', route);

  route.get(
    '/all',
    async (req: Request, res: Response, next: NextFunction) => {
      try {
        const serviceInstance = Container.get(ProviderService);
        const providers = await serviceInstance.getAll();
        return res.json(providers).status(200);
      } catch (e) {
        console.log(' error ', e);
        return next(e);
      }
    }
  );

  route.post(
    '/',
    async (req: Request, res: Response, next: NextFunction) => {
      try {
        const serviceInstance = Container.get(ProviderService);
       
        const newProvider: Provider = req.body;
        const provider = await serviceInstance.createProvider(newProvider);
        return res.json(provider).status(201);
      } catch (e) {
        console.log(' error ', e);
        return next(e);
      }
    }
  );


};