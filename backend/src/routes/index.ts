import { Router } from 'express';
import providers from './providers';
import config from './config';

const router = Router();

providers(router);
config(router);

export default router;