
import { Container } from 'typedi';
import { Request, Response, Router, NextFunction } from 'express';
import InfoService from '../../services/info.service';

const route = Router();
export default (app: Router) => {
  app.use('/info', route);

  route.get(
    '/',
    async (req: Request, res: Response, next: NextFunction) => {
      try {
        const serviceInstance = Container.get(InfoService);
        const info = await serviceInstance.getInfo();
        return res.json(info).status(200);
      } catch (e) {
        console.log(' error ', e);
        return next(e);
      }
    }
  );

};