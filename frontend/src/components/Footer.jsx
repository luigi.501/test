
import React,{useEffect,useState} from 'react';
import api from '../api/api';

function Footer() {
  const [info, setInfo] = useState({});
  useEffect(() => {
    api
      .get("/info")
      .then(res => {
        setInfo(res.data);
      })
      .catch(error => {
        console.log("Error");
      });
  }, []);
    return (
        <footer>
            <p>Versi&oacute;n {info.version}</p>
        </footer>
    );
}

export default Footer;