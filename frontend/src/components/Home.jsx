import React from 'react'
import { Button } from 'react-bootstrap'
import logo from '../assets/logo.png';
import {
 Link
} from "react-router-dom";
const Home = () => {
  
  return (
    <div className='container-home'>
        <div className="centered-image">
            <img src={logo} alt="" />
        </div>
        <Link to="proveedores"><Button>Continuar</Button></Link>
    </div>
  )
}

export default Home