import React,{useEffect,useState} from 'react';
import Grid from '@mui/material/Grid';
import api from '../../api/api';
import Table from '@mui/material/Table';
import TableBody from '@mui/material/TableBody';
import TableCell from '@mui/material/TableCell';
import TableContainer from '@mui/material/TableContainer';
import TableHead from '@mui/material/TableHead';
import TableRow from '@mui/material/TableRow';
import Paper from '@mui/material/Paper';

const ListProviders = ({  }) => {

    const [providers, setProviders] = useState([]);
    useEffect(() => {
        api
          .get("/providers/all")
          .then(res => {
            setProviders(res.data);
            console.log(res.data);
          })
          .catch(error => {
            console.log("Error");
          });
      }, []);

  return (
    <div>
      <h2>Proveedores</h2>
     <div>
    
     <Grid container spacing={2}>
     <TableContainer component={Paper} className='table-container'>
      <Table sx={{ minWidth: 400 }} aria-label="table">
        <TableHead>
          <TableRow>
           
            <TableCell align="right">Nombre</TableCell>
            <TableCell align="right">Raz&oacute;n social</TableCell>
            <TableCell align="right">Direcci&oacute;</TableCell>
          </TableRow>
        </TableHead>
        <TableBody>
          {providers.map((row) => (
            <TableRow
              key={row.name}
              sx={{ '&:last-child td, &:last-child th': { border: 0 } }}
            >
              <TableCell component="th" scope="row">
                {row.name}
              </TableCell>
              <TableCell align="right">{row.businessName}</TableCell>
              <TableCell align="right">{row.address}</TableCell>
          
            </TableRow>
          ))}
        </TableBody>
      </Table>
    </TableContainer>

     </Grid>
    
 

     </div>
    </div>
  );
};

export default ListProviders;