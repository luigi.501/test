import React from 'react';
import {
  BrowserRouter as Router, Route, Routes, Link
} from "react-router-dom";

import Footer from './components/Footer';
import Header from './components/Header';
import Home from './components/Home';
import './App.css';
import ListProviders from './pages/Providers/ListProviders';

function App() {
  return (
    <div className="App">
      <Router>
                
                <main> 
                   <Header/>
                    <Routes>
                        <Route path="/" element={<Home/>}>
                        </Route>
                        <Route path="/proveedores" element={<ListProviders/>} />
                    </Routes>
         
                  <Footer/>
                </main>
               
            </Router>
    </div>
  );
}

export default App;
